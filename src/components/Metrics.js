import React from 'react';
import { Progress, Col, Row, Tooltip } from 'antd';

const getStrokeColor = (percent) => {
  switch (true) {
    case percent < 25:
      return '#a0d911';
    case percent < 50:
      return '#fadb14';
    case percent < 75:
      return '#faad14';
    default:
      return '#fa541c';
  }
}

const LineProgressRow = ({ percent, title }) => {

  return (
    <Row gutter={[0, 24]}>
      <Col span={24}>
        <p>{title}</p>
        <Tooltip title={`${title}: ${percent}%`}>
          <Progress
            percent={percent}
            strokeColor={getStrokeColor(percent)}
            status="normal"
          />
        </Tooltip>
      </Col>
    </Row>
  )
}

const DashboardProgressRow = ({ percent, title }) => {
  return (
    <Row gutter={[0, 12]} justify="end">
      <Col>
        <p>{title}</p>
        <Tooltip title={`${title}: ${percent}%`}>
          <Progress
            type="dashboard"
            percent={percent}
            strokeColor={getStrokeColor(percent)}
            status="normal"
          />
        </Tooltip>
      </Col>
    </Row>
  )
}

const Metrics = ({ gridSize }) => {
  return (
    <div className="metricsDashboardContainer">
      <Row justify="space-between">
        <Col span={12}>
          <LineProgressRow percent={30} title="Score 1" />
          <LineProgressRow percent={20} title="Score 2" />
          <LineProgressRow percent={60} title="Score 3" />
          <LineProgressRow percent={90} title="Score 4" />
        </Col>
        <Col span={12}>
          <Row>
            <Col span={gridSize.width < 500 ? 24 : 12}>
              <DashboardProgressRow percent={30} title="Score 1" />
              <DashboardProgressRow percent={20} title="Score 2" />
            </Col>
            <Col span={gridSize.width < 500 ? 0 : 12}>
              <DashboardProgressRow percent={60} title="Score 3" />
              <DashboardProgressRow percent={90} title="Score 4" />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default Metrics;

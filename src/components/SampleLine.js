import React, { useMemo } from 'react';
import { ResponsiveLine } from '@nivo/line';
import { generateDrinkStats } from '@nivo/generators';


const SampleLine = () => {

  const data = useMemo(() => generateDrinkStats(5), [])

  return (
    <ResponsiveLine
      data={data}
      margin={{ top: 50, right: 20, bottom: 70, left: 45 }}
      xScale={{ type: 'point' }}
      yScale={{ type: 'linear', stacked: false }}
      axisBottom={{
        orient: 'bottom',
        tickSize: 5,
        tickPadding: 5,
        tickRotation: -48,
      }}
      axisLeft={{
        orient: 'left',
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: 'Score',
        legendOffset: -40,
        legendPosition: 'middle'
      }}
      colors={{ scheme: 'category10' }}
      lineWidth={3}
      pointSize={10}
      pointColor={{ theme: 'background' }}
      pointBorderWidth={2}
      pointBorderColor={{ from: 'serieColor' }}
      pointLabel="Score"
      pointLabelYOffset={-12}
      useMesh={true}
      legends={[
        {
          anchor: 'top',
          direction: 'row',
          justify: false,
          translateX: 0,
          translateY: -40,
          itemsSpacing: 0,
          itemDirection: 'left-to-right',
          itemWidth: 80,
          itemHeight: 20,
          itemOpacity: 0.75,
          symbolSize: 12,
          symbolShape: 'circle',
          symbolBorderColor: 'rgba(0, 0, 0, .5)',
          effects: [
            {
              on: 'hover',
              style: {
                itemBackground: 'rgba(0, 0, 0, .03)',
                itemOpacity: 1
              }
            }
          ]
        }
      ]}
    />
  )

}

export default SampleLine;

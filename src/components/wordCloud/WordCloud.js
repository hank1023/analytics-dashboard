import React from 'react';
import ReactWordcloud from "react-wordcloud";

import "tippy.js/dist/tippy.css";
import "tippy.js/animations/scale.css";

import words from "./words";

const options = {
  deterministic: true,
  fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol','Noto Color Emoji'",
  fontWeight: "normal",
}

const WordCloud = () => {
  return (
    <div className="wordCloudContainer">
      <ReactWordcloud
        words={words}
        options={options}
      />
    </div>

  );
}

export default WordCloud;

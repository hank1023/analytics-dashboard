import React, { useState } from 'react';
import { Input, Space, Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';

const SearchableColumn = ({ title, dataIndex, ellipsis, sorter, sortDirections, width, searchPlaceHolder, hasLink, searchKey }) => {
  const [searchText, setSearchText] = useState("");

  const onSearch = (selectedKeys, confirm) => {
    confirm();
    setSearchText(selectedKeys[0]);
  };

  const onSearchFilter = (value, record) => record[searchKey ?? dataIndex].toString().toLowerCase().includes(value.toLowerCase())

  const onReset = clearFilters => {
    clearFilters();
    setSearchText('');
  };

  /* if `hasLink` is set to true, each row should contain a link object consist of {link: ..., description: ...} */
  const searchColumnRender = obj => (
    hasLink ?
      <a href={obj.link} target="_blank" rel="noopener noreferrer">
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={obj.description}
        />
      </a>
      :
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[searchText]}
        autoEscape
        textToHighlight={obj ?? ''}
      />
  );

  return {
    title: title,
    dataIndex: dataIndex,
    ellipsis: ellipsis,
    sorter: sorter,
    sortDirections: sortDirections,
    width: width,
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          id="searchInput"
          placeholder={searchPlaceHolder}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => onSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => onSearch(selectedKeys, confirm)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => onReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: onSearchFilter,
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => document.getElementById('searchInput').focus(), 100);
      }
    },
    render: searchColumnRender,
  };
}

export default SearchableColumn;



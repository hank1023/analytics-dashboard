import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import SearchableColumn from './SearchableColumn';

const SampleTable = ({ gridSize }) => {
  const rowHeight = 40;
  const [pagination, setPagination] = useState(
    {
      current: 1,
      pageSize: 7,
    },
  );

  useEffect(() => {
    // height - 120 is the true height of table body
    let tableBodyHeight = gridSize.height - 140
    let pageSize = Math.floor(tableBodyHeight / rowHeight)
    setPagination(
      {
        current: 1,
        pageSize: pageSize,
      },
    )
  }, [gridSize]);

  const onTableChange = pagination => {
    setPagination(pagination)
  }

  const columns = [
    SearchableColumn({
      title: 'Key Phrases',
      dataIndex: 'keyPhrases',
      key: 'keyPhrases',
      width: '60%',
      ellipsis: true,
      searchPlaceHolder: "Search Key Phrases",
    }),
    {
      title: 'Confidence',
      dataIndex: 'confidence',
      key: 'confidence',
      sorter: (a, b) => a.confidence - b.confidence,
      sortDirections: ['descend', 'ascend'],
      defaultSortOrder: 'descend',
    },
  ];

  const data = [
    {
      key: '1',
      keyPhrases: 'a natural desire',
      confidence: 0.99,
    },
    {
      key: '2',
      keyPhrases: 'uncertainty and anxiety',
      confidence: 0.98,
    },
    {
      key: '3',
      keyPhrases: '2020',
      confidence: 0.97,
    },
    {
      key: '4',
      keyPhrases: 'an anomaly',
      confidence: 0.96,
    },
    {
      key: '5',
      keyPhrases: 'a unique moment',
      confidence: 0.95,
    },
    {
      key: '6',
      keyPhrases: 'a unique moment',
      confidence: 0.95,
    },
    {
      key: '7',
      keyPhrases: 'a unique moment',
      confidence: 0.95,
    },
    {
      key: '8',
      keyPhrases: 'a unique moment',
      confidence: 0.95,
    },
    {
      key: '9',
      keyPhrases: 'a unique moment',
      confidence: 0.95,
    },
    {
      key: '10',
      keyPhrases: 'a unique moment',
      confidence: 0.95,
    },

  ];

  return (
    <Table
      pagination={pagination}
      onChange={onTableChange}
      scroll={{ scrollToFirstRowOnChange: false }}
      columns={columns}
      dataSource={data}
      size="small"
      rowClassName="topicModelTableRow"
    />
  )
};

export default SampleTable;


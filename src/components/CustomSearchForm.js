import React from 'react';
import { Form, Input, DatePicker, Button, Slider } from 'antd';

const CustomSearchForm = ({ onFinish }) => {
  const { RangePicker } = DatePicker;

  const formItemLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  return (
    <Form
      {...formItemLayout}
      name="search_form"
      colon={false}
      onFinish={onFinish}
      className="customSearchForm"
    >
      <Form.Item
        name="field-1"
        label="Field 1"
        rules={[
          {
            required: true,
            message: 'Please input!',
          },
        ]}>
        <Input placeholder="Please input twitter handle" />
      </Form.Item>
      <Form.Item
        name="search-terms"
        label="Search Terms: "
        rules={[
          {
            required: true,
            message: 'Please input search terms!',
          },
        ]}>
        <Input placeholder="Please input search terms" />
      </Form.Item>
      <Form.Item
        name="start-end-date"
        label="Date:"
        rules={[
          {
            type: 'array',
            required: true,
            message: 'Please select start and end dates!',
          },
        ]}>
        <RangePicker />
      </Form.Item>
      <Form.Item
        name="max-no"
        label="Max:"
      >
        <Slider
          marks={{
            0: '0',
            20: '20',
            40: '40',
            60: '60',
            80: '80',
            100: '100',
          }}
        />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 6,
          },
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  )

}

export default CustomSearchForm;

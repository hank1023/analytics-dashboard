import React, { useState, useEffect } from 'react';
import DashboardGridLayout from './DashboardGridLayout';
import ToolbarHeader from './ToolbarHeader';
import { Dropdown, Button, Menu } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import './styles/dashboardComponent.css';
import './styles/styles.css';

const Dashboard = ({ pageTitle, originalItems, itemOptions, colSize, itemMinSize, gridRowHeight }) => {

  const [currentItems, setCurrentItems] = useState({ ...originalItems });
  const [multiElementCount, setMultiElementCount] = useState({});

  const getDefaultLayout = () => {
    var result = {
      lg: [],
      sm: []
    };

    Object.entries(currentItems).forEach(([key, _], i) => {
      var layout = {
        i: key,
        y: Infinity,
        w: itemMinSize.minW,
        h: itemMinSize.minH,
        ...itemMinSize
      };
      result.lg.push({
        ...layout,
        x: (i * itemMinSize.minW) % colSize.lg
      });
      result.sm.push({
        ...layout,
        x: (i * itemMinSize.minW) % colSize.sm
      })
    });
    return result
  };

  const [currentLayouts, setCurrentLayouts] = useState(getDefaultLayout());
  const [newItemKey, setNewItemKey] = useState(null);
  const [removedItemKey, setRemovedItemKey] = useState(null);

  useEffect(() => {
    if (newItemKey === null) return;
    const newElement = document.getElementById(newItemKey)
    if (newElement) {
      newElement.classList.add('newGrid')
      newElement.addEventListener('animationend', () => {
        newElement.classList.remove('newGrid');
      })
    }
    setNewItemKey(null)
  }, [newItemKey])

  useEffect(() => {
    if (removedItemKey === null) return;
    const removedElement = document.getElementById(removedItemKey)
    if (removedElement) {
      removedElement.classList.add('removedGrid')
      removedElement.addEventListener('animationend', () => {
        removedElement.classList.remove('removedGrid')
        delete currentItems[removedItemKey];
        setCurrentItems({ ...currentItems });
      });

    }
    setRemovedItemKey(null)
  }, [removedItemKey, currentItems])

  useEffect(() => {
    // Reset layout to avoid any dangling layouts from deletion
    if (Object.keys(currentItems).length === 0) {
      setCurrentLayouts({
        lg: [],
        sm: []
      })
    }
  }, [currentItems, setCurrentLayouts])

  const onLayoutChange = (_, allLayouts) => {
    console.log(allLayouts)
    setCurrentLayouts(allLayouts);
  }

  const resetLayout = () => {
    setCurrentLayouts(getDefaultLayout());
  }

  const calcNewX = ({ breakpoint }) => {
    const layout = currentLayouts[breakpoint]
    var maxYElement = null;
    var maxLati = 0;

    for (const l of layout) {
      if (!maxYElement || l.y + l.h > maxLati) {
        maxYElement = l
        maxLati = l.y + l.h
      } else if (maxYElement && l.y + l.h === maxLati && l.x > maxYElement.x) {
        maxYElement = l
        maxLati = l.y + l.h
      }
    }

    if (maxYElement && colSize[breakpoint] - (maxYElement.x + maxYElement.w) >= itemMinSize.minW) {
      return maxYElement.x + maxYElement.w;
    } else {
      return 0;
    }
  }

  const generateUniqueKeyTitle = (itemKey) => {
    if (itemOptions.allowOne.includes(itemKey)) return [itemKey, originalItems[itemKey].title]
    const newCount = { ...multiElementCount };
    if (itemKey in newCount) {
      newCount[itemKey] += 1
    } else {
      newCount[itemKey] = 1
    }
    const uniqueKey = itemKey + newCount[itemKey];
    const uniqueTitle = originalItems[itemKey].title + " " + newCount[itemKey];
    setMultiElementCount(newCount);
    return [uniqueKey, uniqueTitle]
  }

  const addItem = (itemKey) => {
    const [uniqueKey, uniqueTitle] = generateUniqueKeyTitle(itemKey);
    const newItem = {}

    newItem[uniqueKey] = {
      title: uniqueTitle,
      component: originalItems[itemKey].component
    }
    const newCurrItems = {
      ...currentItems,
      ...newItem
    }
    setCurrentItems(newCurrItems)

    const newLayout = {
      i: uniqueKey,
      w: itemMinSize.minW,
      h: itemMinSize.minH,
      y: Infinity,
      ...itemMinSize
    };

    const newLgLayout = [
      ...currentLayouts.lg,
      {
        ...newLayout,
        x: calcNewX({ breakpoint: 'lg' }),
      }
    ]

    const newSmLayout = [
      ...currentLayouts.sm,
      {
        ...newLayout,
        x: calcNewX({ breakpoint: 'sm' }),
      }
    ]
    setCurrentLayouts({ lg: newLgLayout, sm: newSmLayout })
    setNewItemKey(uniqueKey)
  }

  const onTagClick = (key) => {
    const element = document.getElementById(key);
    element.scrollIntoView({ block: 'end' })
    element.classList.add('emphasis')
    element.addEventListener('animationend', () => {
      element.classList.remove('emphasis');
    })
  }

  const EmptyAddDropdown = () => {
    const itemSelectMenu = (
      <Menu>
        {
          Object.entries(originalItems).map(([key, item]) => (
            <Menu.Item key={key} onClick={() => addItem(key)}>
              {item.title}
            </Menu.Item>
          ))
        }
      </Menu>
    )

    return (
      <Dropdown overlay={itemSelectMenu} placement="bottomCenter" trigger="click" className="buttonAddLarge">
        <Button icon={<PlusCircleOutlined />} type="primary">
          Add Items
        </Button>
      </Dropdown>
    )
  }

  return (
    <div>
      <ToolbarHeader
        title={pageTitle}
        originalItems={originalItems}
        currentItems={currentItems}
        itemOptions={itemOptions}
        onAdd={addItem}
        onRemove={setRemovedItemKey}
        onReset={resetLayout}
        onTagClick={onTagClick}
      />
      {
        Object.keys(currentItems).length !== 0
          ?
          <DashboardGridLayout
            layouts={currentLayouts}
            cols={colSize}
            rowHeight={gridRowHeight}
            onLayoutChange={onLayoutChange}
            currItems={currentItems}
            onCardDismiss={setRemovedItemKey}
          />
          :
          <EmptyAddDropdown />
      }

    </div>
  );

}

export default Dashboard;

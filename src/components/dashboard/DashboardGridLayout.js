import React, { useMemo } from "react";
import { Responsive, WidthProvider } from "react-grid-layout";
import DashboardCard from './DashboardCard';

const ResponsiveGridLayout = WidthProvider(Responsive);

const DashboardGridLayout = ({ layouts, cols, rowHeight, onLayoutChange, currItems, onCardDismiss }) => {

  const gridItems = useMemo(() => {
    if (!currItems) return {}
    return Object.entries(currItems).map(([key, item]) => {
      return <DashboardCard
        key={key}
        itemKey={key}
        title={item.title}
        item={item.component}
        onCardDismiss={onCardDismiss}
      />
    })
  }, [currItems, onCardDismiss])

  return (
    <ResponsiveGridLayout
      breakpoints={{ lg: 1200, sm: 768 }}
      layouts={layouts}
      cols={cols}
      rowHeight={rowHeight}
      compactType="vertical"
      onLayoutChange={onLayoutChange}
      measureBeforeMount={true}
    >
      {gridItems}
    </ResponsiveGridLayout >
  )
}

export default DashboardGridLayout;

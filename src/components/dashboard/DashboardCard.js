import { Dropdown, Menu } from 'antd';
import { CaretDownFilled } from '@ant-design/icons';
import React from 'react';

const DashboardCard = ({ className, style, onMouseDown, onMouseUp, onTouchEnd, children, item, title, itemKey, onCardDismiss }) => {
  const gridSize = {
    gridSize: {
      width: +style.width.slice(0, -2),
      height: +style.height.slice(0, -2),
    }
  };

  const itemWithSize = React.cloneElement(item, gridSize);

  const dropdownMenu = (
    <Menu>
      <Menu.Item disabled={true}>Reset Size</Menu.Item>
      <Menu.Item danger onClick={() => onCardDismiss(itemKey)}>Discard</Menu.Item>
    </Menu>
  )

  return (
    <div
      className={className}
      style={style}
      onMouseDown={onMouseDown}
      onMouseUp={onMouseUp}
      onTouchEnd={onTouchEnd}
    >
      <div className="gridItemContainer" id={itemKey}>
        <div>
          <h3>
            {title}
            <Dropdown
              className="cardDropdown"
              overlay={dropdownMenu}
              placement="bottomRight"
            >
              <CaretDownFilled />
            </Dropdown>
          </h3>
        </div>
        {itemWithSize}
      </div>
      {children}
    </div>
  )
};

export default DashboardCard;

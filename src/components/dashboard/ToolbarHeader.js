import React from 'react';
import { PageHeader, Menu, Dropdown, Button, Tag } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import { FolderAddOutlined, EllipsisOutlined, PlusCircleFilled } from '@ant-design/icons';

const ToolbarHeader = ({ title, originalItems, itemOptions, currentItems, onSave, onReset, onAdd, onRemove, onTagClick }) => {

  const operationMenu = (
    <Menu>
      <Menu.Item danger onClick={onReset} key="reset">Reset Layout</Menu.Item>
    </Menu>
  )

  const onTagClose = (e, key) => {
    e.preventDefault();
    onRemove(key)
  }

  const itemSelectMenu = (
    <Menu>
      {
        Object.entries(originalItems)
          .filter(entry => (!(entry[0] in currentItems) || itemOptions.allowMultiple.includes(entry[0])))
          .map(([key, item]) => (

            <Menu.Item key={key} onClick={() => onAdd(key)}>
              {item.title}
            </Menu.Item>
          ))
      }
    </Menu>
  )

  const itemsSelect = (
    Object.keys(currentItems).length !== 0
      ?
      <div>
        <p className="tagTitle">Components: </p>
        <TweenOneGroup
          enter={{
            scale: 0.8,
            opacity: 0,
            type: 'from',
            duration: 100,
            onComplete: e => {
              e.target.style = '';
            },
          }}
          leave={{
            opacity: 0,
            width: 0,
            scale: 0,
            duration: 200
          }}
          appear={false}
        >
          {(Object.entries(currentItems).map(([key, item]) => (
            <span key={key} style={{ display: 'inline-block' }}>
              <Tag
                closable
                onClose={(e) => onTagClose(e, key)}
                onClick={() => onTagClick(key)}
              >
                {item.title}
              </Tag>
            </span>
          )))}
          <Dropdown overlay={itemSelectMenu} placement="bottomCenter">
            <PlusCircleFilled />
          </Dropdown>
        </TweenOneGroup>
      </div>
      :
      <></>

  )

  return (
    <PageHeader
      ghost={false}
      title={title}
      extra={[
        <Button
          icon={<FolderAddOutlined />}
          key="save"
          onClick={onSave}
        >
          Save Layout
        </Button>,
        <Dropdown
          overlay={operationMenu}
          key="more"
        >
          <Button icon={<EllipsisOutlined />} />
        </Dropdown>
      ]}
    >
      {itemsSelect}
    </PageHeader>
  )
}

export default ToolbarHeader;

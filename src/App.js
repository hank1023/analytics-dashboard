import React from 'react';
import { Layout, Menu, Button } from 'antd';
import AnalyticalDashboard from './AnalyticalDashboard';
import 'antd/dist/antd.css';
import './App.css';
import { GitlabOutlined } from '@ant-design/icons';

const { Header, Content } = Layout;

const App = () => {
  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['dashboard']}>
          <Menu.Item key="dashboard">Dashboard</Menu.Item>
        </Menu>
        <Button
          ghost
          className="linkButton"
          icon={<GitlabOutlined />}
          href="https://gitlab.com/hank1023/analytics-dashboard"
          target="_blank">View Code on Gitlab</Button>
      </Header>
      <Content>
        <AnalyticalDashboard />
      </Content>
    </Layout>
  );
};

export default App;

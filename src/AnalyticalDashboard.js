import React from 'react';
import Dashboard from './components/dashboard/Dashboard';
import SampleLine from './components/SampleLine';
import WordCloud from './components/wordCloud/WordCloud';
import SampleTable from './components/SampleTable';
import Metrics from './components/Metrics';
import CustomSearchForm from './components/CustomSearchForm';
import "./AnalyticalDashboard.css";
import SampleChord from './components/SampleChord';

const AnalyticalDashboard = () => {
  const colSize = { lg: 12, sm: 8 };
  const itemMinSize = { minW: 4, minH: 7 };
  const gridRowHeight = 50;

  const itemOptions = {
    allowOne: ["lineChart", "wordCloud", "table", "metrics"],
    allowMultiple: ["customSearch"]
  };

  const items = {
    lineChart: {
      title: "Line Chart",
      component: <SampleLine />
    },
    sampleChord: {
      title: "Chord Chart",
      component: <SampleChord />
    },
    wordCloud: {
      title: "Word Cloud",
      component: <WordCloud />
    },
    table: {
      title: "Table",
      component: <SampleTable />
    },
    metrics: {
      title: "Metrics",
      component: <Metrics />
    },
    customSearch: {
      title: "Custom Search",
      component: <CustomSearchForm />
    }
  };

  return (
    <Dashboard
      pageTitle="Analytical Dashboard"
      originalItems={items}
      itemOptions={itemOptions}
      colSize={colSize}
      itemMinSize={itemMinSize}
      gridRowHeight={gridRowHeight}
    />
  );
};

export default AnalyticalDashboard;
